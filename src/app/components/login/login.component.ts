import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent{

  form: FormGroup;
  type: string;
  submit: boolean;
  title = 'Alana Test 1';
  subtitle = 'Acceso de usuarios';

  constructor() {
    this.form = new FormGroup({
      user: new FormControl('', [Validators.required, 
                                 Validators.pattern(/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{1,63}$/)]),
      password: new FormControl('', [Validators.required]),
      type: new FormControl('', [Validators.required])
    });
  }

  access() {
    this.submit = true;
    if (this.form.valid) alert(`Bienvenido ${this.form.controls.user.value} ha ingresado como ${this.translate(this.form.controls.type.value)}`)
  }

  private translate(type: string) {
    return type === 'company' ? 'Empresa' : 'Candidato';
  }


}
